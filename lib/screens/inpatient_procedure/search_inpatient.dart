import 'package:cost_of_care/bloc/inpatient_procedure_bloc/inpatient_procedure_bloc.dart';
import 'package:cost_of_care/models/outpatient_procedure.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'components/inpatient_procedure_list_item.dart';

class SearchInpatient extends SearchDelegate {
  List data;

  SearchInpatient()
      : super(
          searchFieldStyle: TextStyle(
            color: Colors.grey,
            fontSize: 17,
          ),
          searchFieldLabel: "Search Here..",
        );

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
        color: Colors.black,
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        close(context, null);
      },
      icon: Icon(Icons.arrow_back),
      color: Colors.black,
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return data.length == 0
        ? Container(
            child: Center(
                child: Text(
              'No Such Result Found',
              style: TextStyle(fontSize: 20),
            )),
          )
        : Scrollbar(
            child: ListView.builder(
              itemBuilder: (ctx, index) => makeCard(data[index]),
              itemCount: data.length,
            ),
          );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return BlocBuilder<InpatientProcedureBloc, InpatientProcedureState>(
        builder: (BuildContext context, InpatientProcedureState state) {
      if (state is InpatientProcedureLoadedState) {
        List<OutpatientProcedure> inpatientProcedureList =
            state.inpatientProcedures;

        final showSuggestionList = query.isEmpty
            ? inpatientProcedureList.toList()
            : inpatientProcedureList
                .where((inputquery) =>
                    inputquery.name.toString().toLowerCase().contains(query))
                .toList();

        data = showSuggestionList;

        return showSuggestionList.length == 0
            ? Container(
                child: Center(
                    child: Text(
                  'Enter Inpatient Procedure to Search',
                  style: TextStyle(fontSize: 20),
                )),
              )
            : Scrollbar(
                child: ListView.builder(
                  itemBuilder: (ctx, index) =>
                      makeCard(showSuggestionList[index]),
                  itemCount: showSuggestionList.length,
                ),
              );
      }
      return Center(
        child: Container(
          child: Center(child: CircularProgressIndicator()),
        ),
      );
    });
  }
}
